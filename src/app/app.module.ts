import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {AlertModule, ModalModule} from 'ngx-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {SearchComponent} from './search/search.component';
import {ResultComponent} from './result/result.component';
import {DetailComponent} from './detail/detail.component';
import {HttpClientModule} from '@angular/common/http';
import { TagComponent } from './tag/tag.component';
import { QuickWindowComponent } from './quick-window/quick-window.component';
import { QuestionComponent } from './question/question.component';
import { AnswerComponent } from './answer/answer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ResultComponent,
    DetailComponent,
    TagComponent,
    QuickWindowComponent,
    QuestionComponent,
    AnswerComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
