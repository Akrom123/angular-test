import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() question;

  @Output() clickAuthor: EventEmitter<any> = new EventEmitter();
  @Output() clickTag: EventEmitter<any> = new EventEmitter();
  @Output() detail: EventEmitter<any> = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  showAuthorQuestions(id: number) {
    this.clickAuthor.emit(id);
  }

  showTagQuestions(tag: string) {
    this.clickTag.emit(tag);
  }

  emitDetail() {
    this.detail.emit();
  }

}
