import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {QuestionService} from '../question.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html'
})
export class ResultComponent implements OnInit {
  questions: any[];
  query: string;

  loading: boolean = false;

  @ViewChild('quickWindow') quickWindow;

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService
  ) {
    this.route.params.subscribe(routeParams => {
      this.query = routeParams.query;
      if (this.query) {
        this.loading = true;
        this.questionService.getQuestionsByQuery(this.query).subscribe((response: {items: any}) => {
          this.questions = response.items;
          this.loading = false;
        });
      }
    })
  }

  ngOnInit() {
  }

  showTagQuestions(tag) {
    this.quickWindow.open();
    this.questionService.getQuestionsByTag(tag).subscribe((response: {items: any}) => this.quickWindow.setQuestions(response.items));
  }

  showAuthorQuestions(id) {
    this.quickWindow.open();
    this.questionService.getQuestionsByAuthor(id).subscribe((response: {items: any}) => this.quickWindow.setQuestions(response.items));
  }

}
