import {Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {QuestionService} from '../question.service';

@Component({
  selector: 'app-quick-window',
  templateUrl: './quick-window.component.html',
  styleUrls: ['./quick-window.component.css']
})
export class QuickWindowComponent implements OnInit {

  questions: any[];
  config = {
    backdrop: true,
    ignoreBackdropClick: false
  };

  @ViewChild('staticModal') modalRef: BsModalRef;


  constructor(
    private modalService: BsModalService,
    private questionService: QuestionService
  ) { }

  ngOnInit() {
  }

  open() {
    this.modalRef = this.modalService.show(this.modalRef, this.config);
  }

  close() {
    this.modalRef.hide();
  }

  showAuthorQuestions(id) {
    this.questionService.getQuestionsByAuthor(id).subscribe((response: {items: any}) => this.questions = response.items);
  }

  showTagQuestions(tag) {
    this.questionService.getQuestionsByTag(tag).subscribe((response: {items: any}) => this.questions = response.items);
  }

  setQuestions(questions) {
    this.questions = questions;
  }

}
