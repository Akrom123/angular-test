import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {QuestionService} from '../question.service';

interface IDetail {
  tags: string[];
  body: string;
  title: string;
  owner: any;
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html'
})
export class DetailComponent implements OnInit {

  detail: IDetail;

  answers: any[];

  id: number;

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService
  ) {
    this.id = route.snapshot.params.id;
    this.getDetail();
    this.getAnswers();
  }

  ngOnInit() {
  }

  getDetail() {
    this.questionService.getDetail(this.id).subscribe((response: {items: any}) => this.detail = response.items[0]);
  }

  getAnswers() {
    this.questionService.getAnswers(this.id).subscribe((response: {items: any}) => this.answers = response.items);
  }

}
