import {Component} from '@angular/core';
import {setTheme} from 'ngx-bootstrap/utils';
import {fadeAnimation} from './animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation]
})
export class AppComponent {
  constructor() {
    setTheme('bs4'); // or 'bs4'
  }
}
