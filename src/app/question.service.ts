import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  apiUrl = 'https://api.stackexchange.com/2.2';

  constructor(
    private http: HttpClient
  ) {
  }

  getQuestionsByQuery(query: string) {
    const url = `${this.apiUrl}/search?order=desc&sort=activity&site=stackoverflow&intitle=${query}`;
    return this.http.get(url);
  }

  getQuestionsByTag(tag: string) {
    const url = `${this.apiUrl}/search?order=desc&sort=activity&site=stackoverflow&tagged=${tag}`;
    return this.http.get(url);
  }

  getQuestionsByAuthor(authorId: number) {
    const url = `${this.apiUrl}/users/${authorId}/questions?order=desc&sort=activity&site=stackoverflow`;
    return this.http.get(url);
  }

  getDetail(id) {
    const url = `${this.apiUrl}/questions/${id}?site=stackoverflow&filter=!9Z(-wwYCh`;
    return this.http.get(url);
  }

  getAnswers(id) {
    const url = `${this.apiUrl}/questions/${id}/answers?site=stackoverflow&filter=!-*jbN.OXKfDP`;
    return this.http.get(url);
  }
}
